import uuid from 'uuid';
import todoStorage from './todo-local-storage'
import filters from './todo-visibility-filters'
import todoFocus from './todo-focus-directive'
import moment from 'moment'

export default {
  // app initial state
  data() {
    return {
      todos: todoStorage.fetch(),
      newTodo: '',
      editedTodo: null,
    }
  },

  // watch todos change for localStorage persistence
  watch: {
    todos: {
      handler(todos) {
        todoStorage.save(todos)
      },
      deep: true,
    },
  },

  // computed properties
  computed: {
    filteredTodos() {
      return filters[this.visibility](this.todos)
    },
    remaining() {
      return filters.active(this.todos).length
    },
    allDone: {
      get() {
        return this.remaining === 0
      },
      set(value) {
        this.todos.forEach(function(todo) {
          todo.completed = value
        })
      },
    },
    visibility() {
      return (this.$route && this.$route.meta.visibility) || 'all'
    },
  },

  filters: {
    pluralize(n) {
      if( n <= 1 ) return  'item';
      return (n > 5) ? 'or more items' : 'items';
    },
    fiveAndMore(n) {
      return n >= 5 ? 5 : n;
    },
    timeFormat(timeString) {
      return moment(timeString).fromNow();
    }
  },

  // methods that implement data logic.
  // note there's no DOM manipulation here at all.
  methods: {
    addTodo() {
      //get new todo value from frontend
      //add to todos array
      //reset for next input
      this.todos.push({
        id: uuid(),
        completed: false,
        title: this.newTodo,
        created_at: moment().format()
      });
      this.newTodo = '';
    },

    removeTodo(todo) {
      //remove passed todo
      this.todos = this.todos.filter(item => item.id !== todo.id);
    },

    editTodo(todo) {
      //select passed todo for editing
      this.editedTodo = this.todos[this.todos.findIndex(item => item.id === todo.id)];
      this.oldTitle = this.editedTodo.title
    },

    doneEdit(todo) {
      //update passed edited todo
      let todoItemEdited = this.todos[this.todos.findIndex(item => item.id === todo.id)];
      todoItemEdited.title = todo.title;
      this.editedTodo = null;
    },

    cancelEdit(todo) {
      this.editedTodo.title = this.oldTitle;
      this.editedTodo = null;
      //reset passed todo, remove any edits
    },

    removeCompleted() {
      this.todos = filters.active(this.todos)
    },
  },

  directives: {
    'todo-focus': todoFocus,
  },
}
