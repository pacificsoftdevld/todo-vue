import { shallow, mount, createLocalVue } from 'vue-test-utils'
import Component from './todo.vue'
import VueRouter from 'vue-router'

let wrapper

/**
 * Todo component tests
 */
describe('Todo Component', () => {
  beforeEach(() => {
    //https://www.npmjs.com/package/jest-localstorage-mock
    localStorage.clear()
    //https://vue-test-utils.vuejs.org/en/guides/using-with-vue-router.html
    let localVue = createLocalVue()
    localVue.use(VueRouter)
    //https://vue-test-utils.vuejs.org/en/api/shallow.html
    wrapper = shallow(Component, { localVue })
  })
  afterEach(() => {
    wrapper.destroy()
  })

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  describe('Todos', () => {
    test('starts with an empty array of todos', () => {
      expect(wrapper.vm.todos.length).toBe(0)
    })

    describe('New Todo', () => {
      test('adds a todo on enter key', () => {
        wrapper.setData({ newTodo: 'foo' })
        wrapper.find('.new-todo').trigger('keyup.enter')
        expect(wrapper.vm.todos.length).toBe(1)
        expect(wrapper.vm.todos[0].title).toBe('foo')
        expect(wrapper.vm.todos[0].completed).toBe(false)
        expect(wrapper.vm.newTodo).toBe('')
      })

      test('displays the added todo in the list', () => {
        wrapper.setData({ newTodo: 'foo' })
        wrapper.find('.new-todo').trigger('keyup.enter')
        let list = wrapper.find('.todo-list').findAll('li')
        expect(list.length).toBe(1)
        expect(list.at(0).html()).toMatch(/foo/)
      })
    })
  })

  describe('Sections', () => {
    describe('Header', () => {
      test('shows a header', () => {
        expect(wrapper.find('.header>h1').html()).toMatch(/<h1>todos<\/h1>/)
      })

      test('shows a todo input', () => {
        expect(wrapper.find('.header').contains('.new-todo')).toBe(true)
      })
    })

    describe('Main', () => {
      test('shows a main area', () => {
        expect(wrapper.contains('.main')).toBeTruthy()
      })
    })

    describe('Footer', () => {
      test('shows a footer area', () => {
        expect(wrapper.contains('.footer')).toBeTruthy()
      })

      describe('Count', () => {
        test('handles 0 todos', () => {
          wrapper.setData({
            todos: [],
          })
          expect(wrapper.find('.todo-count').html()).toMatch(/0.*items left/)
        })
        test('handles single todo', () => {
          wrapper.setData({
            todos: [{ id: 1, title: 'foo', completed: false }],
          })
          expect(wrapper.find('.todo-count').html()).toMatch(/1.*item left/)
        })
        test('handles multiple todos', () => {
          wrapper.setData({
            todos: [
              { id: 1, title: 'foo', completed: false },
              { id: 2, title: 'bar', completed: false },
            ],
          })
          expect(wrapper.find('.todo-count').html()).toMatch(/2.*items left/)
        })
      })
    })
  })

  describe('Visibility Routing', () => {
    test('defaults to `all`', () => {
      expect(wrapper.vm.visibility).toBe('all')
    })

    test('reads $route.params', () => {
      const $route = {
        meta: {
          visibility: 'active',
        },
      }
      wrapper = shallow(Component, { mocks: { $route } })
      expect(wrapper.vm.visibility).toBe('active')
    })
  })
})
