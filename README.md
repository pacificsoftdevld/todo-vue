# Grade Todo

> A Simple Todo App

** IMPORTANT **

If you are taking this test, please make sure you `fork` this repo before you
start working on it. You will _not_ have permission to push your changes
directly to this repo. Thanks!

## Requirements

* [node](https://nodejs.org/en/)
* [yarn](https://yarnpkg.com)

## Build Setup

```bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build
```

## Other commands

```bash
# lint files
yarn lint

# prettify files
yarn format

# run unit tests
yarn test
```
