/**
 * Todo localStorage persistence
 */
const STORAGE_KEY = 'todos-vuejs-2.0'
let uid = 0
export default {
  uid,
  fetch: function() {
    var todos = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]')
    todos.forEach(function(todo, index) {
      todo.id = index
    })
    uid = todos.length
    return todos
  },
  save: function(todos) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(todos))
  },
}
