/**
 * A custom directive to wait for the DOM to be updated
 * before focusing on the input field.
 * http://vuejs.org/guide/custom-directive.html
 */
export default function(el, binding) {
  if (binding.value) {
    el.focus()
  }
}
