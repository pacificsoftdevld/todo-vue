// component imports
import Todo from './components/todo/todo.vue'

export const routes = [
  {
    path: '/',
    redirect: '/todo',
  },
  {
    path: '/todo',
    component: Todo,
    children: [
      { path: '/todo/active', meta: { visibility: 'active' } },
      { path: '/todo/completed', meta: { visibility: 'completed' } },
    ],
  },
]
